# Notes for PyWeb-IL


## Venue requirements

* A space meant for giving talks (Podium, screen/projector, etc.)
* Free of charge, of course.
* At least 50 seats.
* Our primary location is Tel-Aviv, but we are open to any other place in the country, preferable with easy access with public transportation. Near train stations.
* We would prefer a place that would want to host us regularly, but one-time hosts might be also accepted.
* An optional advantage: Video recording equipment.

## Refreshment

* Pizza or sandwitches or some more interesting snacks. Preferably Kosher with some vegana and some gluten-free options.
* Beer and soft drinks.
* Optionally fruits.
* Optionally sweets.

## Planned dates

Basically once in two months on the first Monday of the month.

* 2024-05-20 (postponed because of holocaust day and memorial day)
* 2024-07-01
* 2024-09-02
* 2024-11-04
