# python.org.il

The site is hosted on GitLab pages. See the pipeline in `.gitlab-ci.yml`.

For historical reasons most of the files are served from the `public/` folder.
Some pages are generated using the `generate.py` prorgam from Markdown files located in the `pages` folder.


## Generate locally

```
pip install -r requirements.txt
python generate.py
```

To view the site locally install [rustatic](https://rustatic.code-maven.com/)

run

```
rustatic --port 5000 --nice --indexfile index.html --path public/
```

and then visit http://localhost:5000/
